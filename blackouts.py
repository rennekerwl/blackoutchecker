import requests
import json


def getIPLocationData(ipAddress):
    """This function takes an IP address as an input. It returns a dictionary of geolocation data as output."""
    base = 'http://freegeoip.net/json/'
    ip = str(ipAddress)
    url = base + ip
    r = requests.get(url)
    data = r.json()

    return data


def getBlackoutsFromZip(zipcode):
    """This function takes a string zipcode as input and returns a list of NHL teams that are blackouted and a list of networks those teams' games are shown on for that zipcode as output."""
    base = 'http://www.nhl.com/ice/ziplookup.htm?q='
    url = base + zipcode
    response = requests.get(url)
    content = str(response.content)
    splitHTML = content.split('>')

    #Find start and end of blackout table
    for line in splitHTML:
        if 'table class="zipTable"' in line:
            tableStart = splitHTML.index(line)
        if 'Nationally televised games on' in line:
            tableEnd = splitHTML.index(line)
    table = splitHTML[tableStart:tableEnd]

    #Get teams and networks for blackout from table
    teamsAndNetworks = []
    for line in table[9:]:
        if '</td' in line:
            teamsAndNetworks.append(line)
    teams = [t.split('<')[0] for t in teamsAndNetworks[0::2]]
    networks = [n.split('<')[0] for n in teamsAndNetworks[1::2]]
    
    return teams, networks


def printBlackouts(teams, networks):
    print('That IP address is blacked out from the following teams: ')
    for team, network in zip(teams, networks):
        print(team, '---', network)



def noZipcode(data):
    """This function takes ip geolocation dictionary as input and prints the best location."""
    if not data['zipcode']:
        print('Zipcode not found, unable to lookup NHL blackouts')
        print('IP address is located in: ' + str(data['country_name']) + ', ' + str(data['region_name']) + ', ' + str(data['city']))
    


def main():
    ip = input('What ip address to check?  ')
    locationData = getIPLocationData(ip)
    if locationData['zipcode']:
        zipcode = locationData['zipcode']
        teams, networks = getBlackoutsFromZip(zipcode)
        printBlackouts(teams, networks)
    else:
        noZipcode(locationData)


if __name__ == '__main__':
    main()
